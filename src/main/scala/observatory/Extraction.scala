package observatory

import java.nio.file.Paths
import java.time.LocalDate

import org.apache.spark.sql.SparkSession

/**
  * 1st milestone: data extraction
  */
object Extraction {

  case class Station(STN: Option[String], WBN: Option[String], location: Option[Location])
  case class Observation(STN: Option[String], WBN: Option[String], date: LocalDate, temperature: Option[Double])

  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("ObservatoryExtraction")
      .config("spark.master", "local")
      .getOrCreate()

  lazy val sc = spark.sparkContext

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Int, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Double)] = {
    //getClass.getResourceAsStream(stationsFile)
    val stationsRdd = sc.textFile(fsPath(stationsFile)).map(toStation)
    val tempsRdd = sc.textFile(fsPath(temperaturesFile)).map(toTemperature(year))

    val goodStationsRdd = stationsRdd
      .filter({case Station(_, _, Some(_)) => true; case Station(_,_, None) => false })
      .map(st => ((st.STN, st.WBN), st))

    val goodObservationsRdd = tempsRdd
      .filter({case Observation(_, _, _, Some(_)) => true; case Observation(_,_, _, None) => false })
      .map(obs => ((obs.STN, obs.WBN), obs))

    goodStationsRdd.join(goodObservationsRdd).mapValues(
      { case (Station(_, _, Some(location)), Observation(_, _, date, Some(temp))) => (date, location, temp)})
      .values.collect().toSeq
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Double)]): Iterable[(Location, Double)] = {
    val recordsRdd = sc.parallelize(records.toSeq).map(tuple => (tuple._2, tuple._3))

    recordsRdd.mapValues((_,1)).reduceByKey {case ((t1,c1),(t2,c2)) => (t1+t2, c1+c2)}
    .mapValues{ case (t,c) => t/c }.collect().toSeq
  }

  def fahrToCel(f: Double): Double = 5.0/9 * (f - 32)

  def toStation(str: String): Station = {
    val arr = str.split(",")
    if (arr.length == 4) Station(toStringOption(arr(0)), toStringOption(arr(1)), toLocationOption(toDoubleOption(arr(2)), toDoubleOption(arr(3))))
    else if (arr.length > 1) Station(toStringOption(arr(0)), toStringOption(arr(1)), None)
    else Station(toStringOption(arr(0)), None, None)
  }

  def toTemperature(year: Int)(str: String): Observation = {
    val arr = str.split(",")
    val temp = arr(4).toDouble
    Observation(
      toStringOption(arr(0)),
      toStringOption(arr(1)),
      LocalDate.of(year,arr(2).toInt, arr(3).toInt),
      if (temp < 9999) Option.apply(fahrToCel(temp)) else None)
  }

  def toStringOption(str: String): Option[String] = if (str.isEmpty) Option.empty else Option.apply(str)

  def toDoubleOption(str: String): Option[Double] = if (str.isEmpty) Option.empty else Option.apply(str.toDouble)

  def toLocationOption(latOpt: Option[Double], longOpt: Option[Double]) = for {lat <- latOpt; lo <- longOpt} yield Location(lat, lo)

  /** @return The filesystem path of the given resource */
  def fsPath(resource: String): String =
    Paths.get(getClass.getResource(resource).toURI).toString

}
