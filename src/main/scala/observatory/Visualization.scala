package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import scala.math._
/**
  * 2nd milestone: basic visualization
  */
object Visualization {

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Double)], location: Location): Double = {
    //println(s"Location is $location")
    val distances = temperatures.map{case (loc, temp) => (distance(loc, location), temp)}
    val closePoint = distances.filter(_._1 < 1)
    if (closePoint.isEmpty) {
      val u = distances
        .map { case (dist, temp) => (temp, 1/math.pow(dist, 2)) }
        .map { case (temp, w) => (temp * w, w) }
        .reduceLeft[(Double, Double)] { case ((n1, w1), (n2, w2)) => (n1 + n2, w1 + w2) }
      u._1 / u._2
    } else closePoint.head._2
  }

  //  def distance(l1: Location, l2: Location): Double = {
  //    val dlambda = l2.lon - l1.lon
  //    Math.acos(Math.sin(l1.lat) * Math.sin(l2.lat) + Math.cos(l2.lat)*Math.cos(l1.lat)*Math.cos(dlambda))*6371
  //  }
  def distance(locA: Location, locB: Location): Double = {
    val Location(latA, lonA) = locA
    val Location(latB, lonB) = locB
    val latDistance = toRadians(latB - latA)
    val lonDistance = toRadians(lonB - lonA)

    val a = pow(sin(latDistance / 2), 2) +
      cos(toRadians(latA)) * cos(toRadians(latB)) *
        pow(sin(lonDistance / 2), 2)

    val c = 2 * atan2(sqrt(a), sqrt(1 - a))
    c * 6371
  }
  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Double, Color)], value: Double): Color = {
    //println(s"Test case $points value $value")
    points.find(_._1 == value) match {
      case Some((_, col)) => col
      case None => {
        val (small, large) = points.toList.sortBy(_._1).partition(_._1 < value)
        (small.reverse.headOption, large.headOption) match {
          case (Some((v1, c1)), Some((v2, c2))) => blendColors (c1, v1, c2, v2, value)
          case (Some((v1, c1)), None) => c1
          case (None, Some((v2, c2))) => c2
          case _ => Color(0,0,0)
        }
      }
    }
  }

  def blendColors(c1: Color, w1: Double, c2: Color, w2: Double, v: Double): Color = {
    val bl = blendFunction(w1, w2, v) _
    Color(
      bl(c1.red, c2.red),
      bl(c1.green, c2.green),
      bl(c1.blue, c2.blue)
    )
  }

  def blendFunction(vMin: Double, vMax: Double, v: Double)(c1: Int, c2: Int) = {
    val fact = (v-vMin)/(vMax - vMin)
    math.round(c1 + fact * (c2 - c1)).toInt
  }
  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Image = {
    val imageWidth = 360
    val imageHeight = 180

    val pixels = (0 until imageHeight * imageWidth).par.map( pos => {
      val color = interpolateColor(colors, predictTemperature(temperatures, posToLocation(imageWidth, imageHeight)(pos)))
      pos -> Pixel(color.red, color.green, color.blue, 255)
    })
      .seq
      .sortBy(_._1)
      .map(_._2)
      .toArray
    Image(360, 180, pixels)
  }

  def locationToIndex(location: Location): (Int, Int) = (location.lon.toInt+180, location.lat.toInt+90)

  def indexTolocation(x:Int, y:Int): Location = Location(x - 180, y - 90)

  def posToLocation(imageWidth: Int, imageHeight: Int)(pos: Int): Location = {
    val widthFactor = 180 * 2 / imageWidth.toDouble
    val heightFactor = 90 * 2 / imageHeight.toDouble

    val x: Int = pos % imageWidth
    val y: Int = pos / imageWidth

    Location(90 - (y * heightFactor), (x * widthFactor) - 180)
  }
}

