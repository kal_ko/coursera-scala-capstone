package observatory


import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers

@RunWith(classOf[JUnitRunner])
class VisualizationTest extends FunSuite with Checkers {

  lazy val locateTemperatures = Extraction.locateTemperatures(year, stationsPath, temperaturePath)
  lazy val locateAverage = Extraction.locationYearlyAverageRecords(locateTemperatures)
  val year = 1975
  val debug = true
  val stationsPath: String = "/stations.csv"
//  val temperaturePath: String = s"/$year-50ksample.csv"
  val temperaturePath: String = s"/$year.csv"


  test("locationYearlyAverageRecords") {
    if (debug) locateAverage.take(20).foreach(println)
    assert(locateAverage.count(_._1 == Location(70.933, -8.667)) === 1)
    assert(locateAverage.size === 181)
  }

  test("test interpolation") {
    val points = List((60.0	, Color(255	,255	,255   )),
      (32.0	, Color(255	,0	,0     )),
      (12.0	, Color(255	,255	,0     )),
      (0.0	, Color(0	,255	,255   )),
      (-15.0	, Color(0	,0	,255   )),
      (-27.0	, Color(255	,0	,255   )),
      (-50.0	, Color(33	,0	,107   )),
      (-60.0	, Color(0	,0	,0     )))

    assert(Color(0	,255	,255) == Visualization.interpolateColor(points, 0.0))
    assert(Color(128	,255	, 128) == Visualization.interpolateColor(points, 6.0))
    println(Visualization.interpolateColor(points, 100.0))
    println(Visualization.interpolateColor(points, -100.0))
    println(Visualization.interpolateColor(List((-32.32985632199754,Color(255,0,0)), (-46.422745734831516,Color(0,0,255))), -44.338225289983136))
  }

  test("interpolateColor") {
    val palette = List(
      (100.0, Color(255, 255, 255)),
      (50.0, Color(0, 0, 0)),
      (0.0, Color(255, 0, 128))
    )

    assert(Visualization.interpolateColor(palette, 50.0) === Color(0, 0, 0))
    assert(Visualization.interpolateColor(palette, 0.0) === Color(255, 0, 128))
    assert(Visualization.interpolateColor(palette, -10.0) === Color(255, 0, 128))
    assert(Visualization.interpolateColor(palette, 200.0) === Color(255, 255, 255))
    assert(Visualization.interpolateColor(palette, 75.0) === Color(128, 128, 128))
    assert(Visualization.interpolateColor(palette, 25.0) === Color(128, 0, 64))
  }

  test("test location") {
    val locations = List(
      (Location(10, -10), 50.0),
      (Location(-10, -10), 0.0),
      (Location(-10, 10), 0.0),
      (Location(10, 10), 50.0)
    )

    println(Visualization.predictTemperature(locations, Location(10.0, 10.001)))
  }


  test("predictTemperature small sets") {
    assert(Visualization.predictTemperature(List((Location(45.0, -90.0), 10.0), (Location(-45.0, 0.0), 20.0)), Location(0.0, -45.0)) === 15.0)
    assert(Visualization.predictTemperature(List((Location(0.0, 0.0), 10.0)), Location(0.0, 0.0)) === 10.0)
    assert(Visualization.predictTemperature(List((Location(45.0, -90.0), 0.0), (Location(-45.0, 0.0), 59.028308521858634)), Location(0.0, 0.0)).round === 45)
  }

  test("print image") {
    val locations = List(
      (Location(10, -10), 50.0),
      (Location(-10, -10), 0.0),
      (Location(-10, 10), 0.0),
      (Location(10, 10), 50.0)
    )

    val colors = List((60.0	, Color(255	,255	,255   )),
      (32.0	, Color(255	,0	,0     )),
      (12.0	, Color(255	,255	,0     )),
      (0.0	, Color(0	,255	,255   )),
      (-15.0	, Color(0	,0	,255   )),
      (-27.0	, Color(255	,0	,255   )),
      (-50.0	, Color(33	,0	,107   )),
      (-60.0	, Color(0	,0	,0     )))

    val img = Visualization.visualize(locateAverage, colors)

    img.output(new java.io.File("target/some-image.png"))
  }
}
