package observatory

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExtractionTest extends FunSuite {

  test("executable") {
    val executable = try {
      Extraction.locateTemperatures(1975, "/stations.csv", "/1975.csv")
      true
    } catch {
      case _: Throwable => false
    }
    assert(executable, "Can't run simple test")
  }

  test("test simple files") {
    val temps = Extraction.locateTemperatures(2015, "/stations_test.csv", "/2015_test.csv")
    val averTemps = Extraction.locationYearlyAverageRecords(temps)

    println(temps)
    println(averTemps)

  }
}